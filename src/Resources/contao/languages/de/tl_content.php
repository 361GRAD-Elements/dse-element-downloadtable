<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']            = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_start'] = ['Download Tabelle Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_entry'] = ['Download Tabelle Eintrag', ''];
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_stop']  = ['Download Tabelle Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['download_legend']      = 'Download Tabelle Eintrag';
$GLOBALS['TL_LANG']['tl_content']['downloadtable_legend'] = 'Download Tabelle';

$GLOBALS['TL_LANG']['tl_content']['dse_downloadtable_head'] = ['Tabellen Title', 'Geben Sie einen Titel für diese Download Tabelle ein.'];
$GLOBALS['TL_LANG']['tl_content']['dse_downloadentry']      = ['Datei', 'Wählen Sie eine Datei für den Download aus.'];
$GLOBALS['TL_LANG']['tl_content']['dse_downloadtitle']      =
    ['Datei Titel', 'Geben Sie einen passenden Titel für diesen Download an.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];