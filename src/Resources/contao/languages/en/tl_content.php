<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']            = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_start'] = ['Download Table Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_entry'] = ['Download Table Entry', ''];
$GLOBALS['TL_LANG']['CTE']['dse_downloadtable_stop']  = ['Download Table Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['download_legend']      = 'Download Table Entry';
$GLOBALS['TL_LANG']['tl_content']['downloadtable_legend'] = 'Download Table';

$GLOBALS['TL_LANG']['tl_content']['dse_downloadtable_head'] = ['Table title', 'Enter a title for this download list.'];
$GLOBALS['TL_LANG']['tl_content']['dse_downloadentry']      = ['File', 'Choose file to generate download.'];
$GLOBALS['TL_LANG']['tl_content']['dse_downloadtitle']      =
    ['File title', 'Enter suitable title for the download.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];