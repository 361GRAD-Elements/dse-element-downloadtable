<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_downloadtable_start'] =
    'Dse\\ElementsBundle\\ElementDownloadTable\\Element\\ContentDseDownloadTableStart';
$GLOBALS['TL_CTE']['dse_elements']['dse_downloadtable_entry'] =
    'Dse\\ElementsBundle\\ElementDownloadTable\\Element\\ContentDseDownloadTableEntry';
$GLOBALS['TL_CTE']['dse_elements']['dse_downloadtable_stop']  =
    'Dse\\ElementsBundle\\ElementDownloadTable\\Element\\ContentDseDownloadTableStop';

$GLOBALS['TL_WRAPPERS']['start'][] = 'dse_downloadtable_start';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'dse_downloadtable_stop';
