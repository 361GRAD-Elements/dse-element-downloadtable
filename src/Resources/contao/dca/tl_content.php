<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_downloadtable_start'] =
    '{type_legend},type,headline,dse_secondline,dse_subheadline;' .
    '{downloadtable_legend},dse_downloadtable_head;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_downloadtable_entry'] =
    '{type_legend},type;' .
    '{download_legend},dse_downloadentry,dse_downloadtitle;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_downloadtable_stop']  =
    '{type_legend},type;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests;' .
    '{invisible_legend:hide},invisible,start,stop';


// ELement fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_downloadtable_head'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_downloadtable_head'],
    'inputType' => 'text',
    'search'    => true,
    'eval'      => [
        'maxlenght' => 200,
        'tl_class'  => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_downloadentry'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_downloadentry'],
    'inputType' => 'fileTree',
    'eval'      => [
        'filesOnly'  => true,
        'fieldType'  => 'radio',
        'extensions' => 'jpg,jpeg,png,gif,svg,pdf',
        'style'      => 'width:200px;'
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_downloadtitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_downloadtitle'],
    'inputType' => 'text',
    'search'    => true,
    'eval'      => [
        'maxlenght' => 200,
        'tl_class'  => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];